# For future improvement
# https://github.com/horejsek/python-webapp-example/blob/master/Makefile
# https://gist.github.com/lumengxi/0ae4645124cd4066f676
# https://github.com/mwilliamson/python-makefile/blob/master/makefile
HOST=0.0.0.0
PORT=8080
PROJECT_NAME=sdr-srv

.PHONY: all lint pretty clean

all: test

check: lint test radon bandit

# We use both because they find fixes after each other
pretty: autopep8 yapf

venv: requirements.txt
	pip3 install -r requirements.txt

lint: venv
	. venv/bin/activate && flake8

test: venv
	coverage run --branch --omit="venv/*,tests/*,tests.py" tests.py && \
	coverage report && \
	coverage html --fail-under=70 && \
	coverage erase

isort:
	sh -c "isort --skip-glob=.tox --recursive . "

autopep8: venv
	. venv/bin/activate && \
	# TODO: add venv/* and migrations/* exclusion
	autopep8 --in-place --aggressive --aggressive -r .

yapf: venv
	. venv/bin/activate && \
	# TODO: -e "venv/*|migrations/*" not working
	yapf -ir -e "venv/*|migrations/*" .

radon: venv
	. venv/bin/activate && \
	radon cc -s -a --ignore='venv,migrations' . && \
	radon mi -s --ignore='venv,migrations' .

bandit: venv
	. venv/bin/activate && \
	bandit -r --exclude='./venv/,./migrations/' .

run:
	# python manage.py runserver
	python3 -m flask upgrade && python3 -m flask run --host=$(HOST) --port=$(PORT)

docker-build:
	docker build \
		--file=./Dockerfile \
		--tag=$(PROJECT_NAME) ./
docker-run:	
	docker run \
		--detach=true \
		--name=$(PROJECT_NAME) \
		--publish=$(PORT) \
		$(PROJECT_NAME)

clean:
	rm -rf venv
	rm -rf htmlcov
	find -iname "*.pyc" -delete
	find -type d -name __pycache__ -delete
